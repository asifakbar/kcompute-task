﻿using Microsoft.EntityFrameworkCore;
using Models.DomainModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace LMSContext.Context
{
    public class LMSDBContext:DbContext
    {
        public LMSDBContext(DbContextOptions<LMSDBContext> options):base(options)
        {
        }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {

        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Seed();

            modelBuilder.Entity<Borrowing>()
                .HasOne(a => a.Penalty)
                .WithOne(b => b.Borrowing)
                .HasForeignKey<Penalty>(c => c.BorrowingId);

        }
        public DbSet<Book> Books { get; set; }
        public DbSet<Borrower> Borrowers { get; set; }
        public DbSet<Library> Libraries { get; set; }
        public DbSet<Penalty> Penalties { get; set; }
        public DbSet<Borrowing> Borrowings { get; set; }


    }
}
