﻿using Microsoft.EntityFrameworkCore;
using Models.DomainModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace LMSContext.Context
{
    public static class DbSeed
    {
        public static void Seed(this ModelBuilder builder)
        {
            builder.Entity<Library>().HasData(
                new Library
                {
                    LibraryId=1,
                    LibraryName = "New Library 1"
                },
                new Library
                {
                    LibraryId =2,
                    LibraryName = "New Library 2"
                });
            builder.Entity<Book>().HasData(
                new Book
                {
                    LibraryId = 1,
                    BookName = "Book 1",
                    BookGenreEnum = BookGenreEnum.History,
                    BookAuthor = "Author 1",
                    BookId = 1,
                    IsBookAvailable = BookAvailablity.Borrowed
                },
                new Book
                {
                    LibraryId = 1,
                    BookName = "Book 2",
                    BookGenreEnum = BookGenreEnum.Horro,
                    BookAuthor = "Author 2",
                    BookId=2
                },
                new Book
                {
                    LibraryId = 2,
                    BookName = "Book 3",
                    BookGenreEnum = BookGenreEnum.IT,
                    BookAuthor = "Author 3",
                    BookId=3
                },
                new Book
                {
                    LibraryId = 2,
                    BookName = "Book 4",
                    BookGenreEnum = BookGenreEnum.Literature,
                    BookAuthor = "Author 4",
                    BookId =4,
                    IsBookAvailable= BookAvailablity.Borrowed
                }
                );
            builder.Entity<Borrower>().HasData(
                new Borrower
                {
                    BorrowerId = 1,
                    BorrowerName = "Borrower 1"
                },
                new Borrower
                {
                    BorrowerId=2,
                    BorrowerName = "Borrower 2",
                    BorrowerGeovernmentIdentity = Guid.NewGuid()
                },
                new Borrower
                {
                    BorrowerId =3,
                    BorrowerName = "Borrower 3",
                    BorrowerGeovernmentIdentity = Guid.NewGuid()
                }
                );
            var borrowing1 = new Borrowing();
            borrowing1.BorrowingId = 1;
            borrowing1.StartDate = DateTime.Today.AddDays(-5);
            borrowing1.EndDate = borrowing1.StartDate.GetEndDateAfterSevenBusinessDays();
            borrowing1.BookId = 1;
            borrowing1.BorrowerId = 1;
            builder.Entity<Borrowing>().HasData(borrowing1);

            var borrowing2 = new Borrowing();
            borrowing2.BorrowingId = 2;
            borrowing2.StartDate = DateTime.Today.AddDays(-2);
            borrowing2.EndDate = borrowing2.StartDate.GetEndDateAfterSevenBusinessDays();
            borrowing2.BookId = 4;
            borrowing2.BorrowerId = 2;
            builder.Entity<Borrowing>().HasData(borrowing2);
            //new Borrowing
            //    {
            //        BorrowingId = 2,
            //        StartDate = DateTime.Today.AddDays(-1),
            //        EndDate =
            //        BookId = 4,
            //        BorrowerId = 2
            //    }
            //    );
        }
    }
}
