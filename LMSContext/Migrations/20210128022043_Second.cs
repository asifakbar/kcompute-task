﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace LMSContext.Migrations
{
    public partial class Second : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<DateTime>(
                name: "EndDate",
                table: "Borrowings",
                type: "datetime2",
                nullable: true,
                oldClrType: typeof(DateTime),
                oldType: "datetime2");

            migrationBuilder.UpdateData(
                table: "Borrowers",
                keyColumn: "BorrowerId",
                keyValue: 2,
                column: "BorrowerGeovernmentIdentity",
                value: new Guid("a77cb4f0-7dee-4147-88b4-3f2b5a4456e1"));

            migrationBuilder.UpdateData(
                table: "Borrowers",
                keyColumn: "BorrowerId",
                keyValue: 3,
                column: "BorrowerGeovernmentIdentity",
                value: new Guid("be43d6b4-2143-4bee-9793-d8eb49b2bfa5"));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<DateTime>(
                name: "EndDate",
                table: "Borrowings",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldNullable: true);

            migrationBuilder.UpdateData(
                table: "Borrowers",
                keyColumn: "BorrowerId",
                keyValue: 2,
                column: "BorrowerGeovernmentIdentity",
                value: new Guid("776e8cac-1335-4eb3-a9dd-c2146827bb7a"));

            migrationBuilder.UpdateData(
                table: "Borrowers",
                keyColumn: "BorrowerId",
                keyValue: 3,
                column: "BorrowerGeovernmentIdentity",
                value: new Guid("96cbe0fe-b9fe-4326-b1b1-1072bac2d5dd"));
        }
    }
}
