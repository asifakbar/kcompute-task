﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace LMSContext.Migrations
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Borrowers",
                columns: table => new
                {
                    BorrowerId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    BorrowerName = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    BorrowerGeovernmentIdentity = table.Column<Guid>(type: "uniqueidentifier", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Borrowers", x => x.BorrowerId);
                });

            migrationBuilder.CreateTable(
                name: "Libraries",
                columns: table => new
                {
                    LibraryId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    LibraryName = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    LibraryAddress = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Libraries", x => x.LibraryId);
                });

            migrationBuilder.CreateTable(
                name: "Books",
                columns: table => new
                {
                    BookId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    BookName = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    BookAuthor = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    BookGenreEnum = table.Column<byte>(type: "tinyint", nullable: false),
                    IsBookAvailable = table.Column<byte>(type: "tinyint", nullable: false),
                    LibraryId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Books", x => x.BookId);
                    table.ForeignKey(
                        name: "FK_Books_Libraries_LibraryId",
                        column: x => x.LibraryId,
                        principalTable: "Libraries",
                        principalColumn: "LibraryId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Borrowings",
                columns: table => new
                {
                    BorrowingId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    StartDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    EndDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    ReceivedDate = table.Column<DateTime>(type: "datetime2", nullable: true),
                    BookId = table.Column<int>(type: "int", nullable: false),
                    BorrowerId = table.Column<int>(type: "int", nullable: false),
                    PenaltyId = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Borrowings", x => x.BorrowingId);
                    table.ForeignKey(
                        name: "FK_Borrowings_Books_BookId",
                        column: x => x.BookId,
                        principalTable: "Books",
                        principalColumn: "BookId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Borrowings_Borrowers_BorrowerId",
                        column: x => x.BorrowerId,
                        principalTable: "Borrowers",
                        principalColumn: "BorrowerId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Penalties",
                columns: table => new
                {
                    PenaltyId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    PenaltyDays = table.Column<byte>(type: "tinyint", nullable: false),
                    PenaltyAmount = table.Column<int>(type: "int", nullable: false),
                    BorrowingId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Penalties", x => x.PenaltyId);
                    table.ForeignKey(
                        name: "FK_Penalties_Borrowings_BorrowingId",
                        column: x => x.BorrowingId,
                        principalTable: "Borrowings",
                        principalColumn: "BorrowingId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "Borrowers",
                columns: new[] { "BorrowerId", "BorrowerGeovernmentIdentity", "BorrowerName" },
                values: new object[,]
                {
                    { 1, new Guid("00000000-0000-0000-0000-000000000000"), "Borrower 1" },
                    { 2, new Guid("776e8cac-1335-4eb3-a9dd-c2146827bb7a"), "Borrower 2" },
                    { 3, new Guid("96cbe0fe-b9fe-4326-b1b1-1072bac2d5dd"), "Borrower 3" }
                });

            migrationBuilder.InsertData(
                table: "Libraries",
                columns: new[] { "LibraryId", "LibraryAddress", "LibraryName" },
                values: new object[,]
                {
                    { 1, null, "New Library 1" },
                    { 2, null, "New Library 2" }
                });

            migrationBuilder.InsertData(
                table: "Books",
                columns: new[] { "BookId", "BookAuthor", "BookGenreEnum", "BookName", "IsBookAvailable", "LibraryId" },
                values: new object[,]
                {
                    { 1, "Author 1", (byte)4, "Book 1", (byte)1, 1 },
                    { 2, "Author 2", (byte)2, "Book 2", (byte)0, 1 },
                    { 3, "Author 3", (byte)6, "Book 3", (byte)0, 2 },
                    { 4, "Author 4", (byte)0, "Book 4", (byte)1, 2 }
                });

            migrationBuilder.InsertData(
                table: "Borrowings",
                columns: new[] { "BorrowingId", "BookId", "BorrowerId", "EndDate", "PenaltyId", "ReceivedDate", "StartDate" },
                values: new object[] { 1, 1, 1, new DateTime(2021, 2, 2, 0, 0, 0, 0, DateTimeKind.Local), null, null, new DateTime(2021, 1, 23, 0, 0, 0, 0, DateTimeKind.Local) });

            migrationBuilder.InsertData(
                table: "Borrowings",
                columns: new[] { "BorrowingId", "BookId", "BorrowerId", "EndDate", "PenaltyId", "ReceivedDate", "StartDate" },
                values: new object[] { 2, 4, 2, new DateTime(2021, 2, 4, 0, 0, 0, 0, DateTimeKind.Local), null, null, new DateTime(2021, 1, 26, 0, 0, 0, 0, DateTimeKind.Local) });

            migrationBuilder.CreateIndex(
                name: "IX_Books_LibraryId",
                table: "Books",
                column: "LibraryId");

            migrationBuilder.CreateIndex(
                name: "IX_Borrowings_BookId",
                table: "Borrowings",
                column: "BookId");

            migrationBuilder.CreateIndex(
                name: "IX_Borrowings_BorrowerId",
                table: "Borrowings",
                column: "BorrowerId");

            migrationBuilder.CreateIndex(
                name: "IX_Penalties_BorrowingId",
                table: "Penalties",
                column: "BorrowingId",
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Penalties");

            migrationBuilder.DropTable(
                name: "Borrowings");

            migrationBuilder.DropTable(
                name: "Books");

            migrationBuilder.DropTable(
                name: "Borrowers");

            migrationBuilder.DropTable(
                name: "Libraries");
        }
    }
}
