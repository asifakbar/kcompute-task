﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace LMSContext.Migrations
{
    public partial class Third : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<int>(
                name: "PenaltyDays",
                table: "Penalties",
                type: "int",
                nullable: false,
                oldClrType: typeof(byte),
                oldType: "tinyint");

            migrationBuilder.UpdateData(
                table: "Borrowers",
                keyColumn: "BorrowerId",
                keyValue: 2,
                column: "BorrowerGeovernmentIdentity",
                value: new Guid("82091a2e-6219-47b1-83d3-057b8c96f273"));

            migrationBuilder.UpdateData(
                table: "Borrowers",
                keyColumn: "BorrowerId",
                keyValue: 3,
                column: "BorrowerGeovernmentIdentity",
                value: new Guid("12715e1b-d18f-41f2-b680-bb073a8fa53d"));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<byte>(
                name: "PenaltyDays",
                table: "Penalties",
                type: "tinyint",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.UpdateData(
                table: "Borrowers",
                keyColumn: "BorrowerId",
                keyValue: 2,
                column: "BorrowerGeovernmentIdentity",
                value: new Guid("a77cb4f0-7dee-4147-88b4-3f2b5a4456e1"));

            migrationBuilder.UpdateData(
                table: "Borrowers",
                keyColumn: "BorrowerId",
                keyValue: 3,
                column: "BorrowerGeovernmentIdentity",
                value: new Guid("be43d6b4-2143-4bee-9793-d8eb49b2bfa5"));
        }
    }
}
