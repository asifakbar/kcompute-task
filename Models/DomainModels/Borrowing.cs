﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Models.DomainModels
{
    public class Borrowing
    {
        [Key,DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int BorrowingId { get; set; }

        [DataType(DataType.Date)]
        [Required,Display(Name = "Borrowing Start Date")]
        public DateTime StartDate { get; set; }

        [Display(Name = "Borrowing End Date (7 Business Days)")]
        public DateTime? EndDate { get; set; }

        [Display(Name = "Book Receiving Date"), DataType(DataType.Date)]
        public DateTime? ReceivedDate { get; set; }
        public int BookId { get; set; }
        public Book Book { get; set; }
        public int BorrowerId { get; set; }
        public Borrower Borrower { get; set; }

        public int? PenaltyId { get; set; }
        public Penalty Penalty { get; set; }

    }
    public static class BorrowingEndDateExtension
    {
        public static DateTime GetEndDateAfterSevenBusinessDays(this DateTime startDate, int days = 7)
        {
            int counter = 0;
            DateTime iterate = startDate.Date;
            //Console.WriteLine($"Starting Date is {startDate.Date} and Day is {startDate.DayOfWeek}");
            while (counter < days)
            {
                iterate = iterate.AddDays(1);
                if (iterate.DayOfWeek == DayOfWeek.Saturday || iterate.DayOfWeek == DayOfWeek.Sunday)
                {
                    //Console.WriteLine($"{iterate.DayOfWeek} is a weekend having date {iterate.Date}");
                }
                else
                {
                    //Console.WriteLine($"Todays Date is {iterate.Date} and Day is {iterate.DayOfWeek}");
                    counter++;
                    //Console.WriteLine($"Counter Value is {counter}");
                }
            }
            return iterate;
            //Console.WriteLine($"After 7 Days, Date will be: {iterate.Date} and day will be: {iterate.Day}. Iterator: {iterate}");
        }
    }

    public static class BorrowingStartDayExtension
    {
        public static bool IsStartingDayAWeekender(this DateTime startDate)
        {
            return startDate.Date.DayOfWeek == DayOfWeek.Saturday || startDate.Date.DayOfWeek == DayOfWeek.Sunday;
        }
    }
}
