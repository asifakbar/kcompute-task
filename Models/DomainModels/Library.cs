﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Models.DomainModels
{
    public class Library
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int LibraryId { get; set; }

        [Required,Display(Name = "Library Name"), MaxLength(100)]
        public string LibraryName { get; set; }

        [Display(Name = "Library Address"), MaxLength(200)]
        public string LibraryAddress { get; set; }
        public ICollection<Book> Books { get; set; }
    }
}
