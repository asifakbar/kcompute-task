﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Models.DomainModels
{
    public class Borrower
    {
        [Key,DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int BorrowerId { get; set; }

        [Required,MaxLength(100),Display(Name = "Borrower Name")]
        public string BorrowerName { get; set; }

        [Display(Name = "Government Issued ID")]
        public Guid BorrowerGeovernmentIdentity { get; set; }
    }
}
