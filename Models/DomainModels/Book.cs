﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Models.DomainModels
{
    public class Book
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int BookId { get; set; }

        [Required,MaxLength(100),Display(Name = "Book Name")]
        public string BookName { get; set; }

        [Required, MaxLength(100),Display(Name ="Author Name")]
        public string BookAuthor { get; set; }

        [Required,EnumDataType(typeof(BookGenreEnum)),Display(Name ="Genre")]
        public BookGenreEnum BookGenreEnum { get; set; }

        [Required, EnumDataType(typeof(BookAvailablity)), Display(Name = "Book Availability")]
        public BookAvailablity IsBookAvailable { get; set; } = BookAvailablity.Available;

        public int LibraryId { get; set; }
        public Library Library { get; set; }
    }

    public enum BookGenreEnum:byte
    {
        Literature,Scifi,Horro,Documentary,History,Medicine,IT
    }

    public enum BookAvailablity:byte
    {
        Available,Borrowed
    }
}
