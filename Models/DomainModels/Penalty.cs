﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Models.DomainModels
{
    public class Penalty
    {
        [Key,DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int PenaltyId { get; set; }

        [Required,Display(Name ="Penalty Incurred For Days: ")]
        public int PenaltyDays { get; set; }

        [Required, Display(Name = "Penalty Amount Incurred: ")]
        public int PenaltyAmount { get; set; }

        public int BorrowingId { get; set; }
        public Borrowing Borrowing { get; set; }
    }


}
