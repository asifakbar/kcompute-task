﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.Controllers
{
    public class BookController : Controller
    {
        private readonly LMSContext.Context.LMSDBContext _context;

        public BookController(LMSContext.Context.LMSDBContext context)
        {
            _context = context;
        }
        // GET: BookController
        public async Task<IActionResult> Index()
        {
            return View(await _context.Books.Include(a=>a.Library).ToListAsync());
        }

        // GET: BookController/Details/5
        public async Task<IActionResult> Details(int id)
        {
            //var _book = await _context.Books.FindAsync(id);
            return View(await _context.Books.FindAsync(id));
        }

        // GET: BookController/Create
        public async Task<IActionResult> Create()
        {
            var libraries = await _context.Libraries.ToListAsync();
            libraries.Insert(0, new Models.DomainModels.Library { LibraryId = 0, LibraryName = "Select one library" });
            ViewBag.LibraryList = libraries;
            return View();
        }

        // POST: BookController/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(Models.DomainModels.Book book)
        {
            if (book.LibraryId == 0)
            {
                ModelState.AddModelError("", $"Error: Please select a library");
                var libraries = await _context.Libraries.ToListAsync();
                libraries.Insert(0, new Models.DomainModels.Library { LibraryId = 0, LibraryName = "Select one library" });
                ViewBag.LibraryList = libraries;
                return View(book);
            }
            if (ModelState.IsValid)
            {
                try
                {
                    await _context.Books.AddAsync(book);
                    await _context.SaveChangesAsync();
                    return RedirectToAction(nameof(Index));
                }
                catch( Exception ex)
                {
                    ModelState.AddModelError("", $"Error: {ex.Message}");
                    return View(book);
                }
            }
            return View(book);
        }

        // GET: BookController/Edit/5
        public async Task<IActionResult> Edit(int id)
        {
            return View(await _context.Books.FindAsync(id));
        }

        // POST: BookController/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, Models.DomainModels.Book book)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    _context.Entry(book).State = EntityState.Modified;
                    await _context.SaveChangesAsync();
                    return RedirectToAction(nameof(Index));
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError("", $"Error: {ex.Message}");
                    return View(book);
                    //throw;
                }
            }
            return View(book);
        }

        // GET: BookController/Delete/5
        public async Task<IActionResult> Delete(int id)
        {
            return View(await _context.Books.FindAsync(id));
        }

        // POST: BookController/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, Models.DomainModels.Book book)
        {
            try
            {
                //var result = await _context.Libraries.FindAsync(library.LibraryId);
                //if (result == null)
                //{
                //    return NotFound();
                //}
                //_context.Libraries.Remove(result);
                //await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", $"Error: {ex.Message}");
                return View();
            }
        }
    }
}
