﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.Controllers
{
    public class LibraryController : Controller
    {
        private readonly LMSContext.Context.LMSDBContext _context;

        public LibraryController(LMSContext.Context.LMSDBContext context)
        {
            _context = context;
        }
        // GET: LibraryController
        public async Task<ActionResult> Index()
        {
            return View(await _context.Libraries.ToListAsync());
        }

        // GET: LibraryController/Details/5
        public async Task<IActionResult> Details(int id)
        {
            var result = await _context.Libraries.FindAsync(id);
            return View(result);
        }

        // GET: LibraryController/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: LibraryController/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(Models.DomainModels.Library library)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    await _context.Libraries.AddAsync(library);
                    await _context.SaveChangesAsync();
                    return RedirectToAction(nameof(Index));
                }
                catch(Exception ex)
                {
                    ModelState.AddModelError("", $"Error: {ex.Message}");
                    return View(library);
                    //throw;
                }
            }
            return View(library);
        }

        // GET: LibraryController/Edit/5
        public async Task<IActionResult> Edit(int id)
        {
            return View(await _context.Libraries.FindAsync(id));
        }

        // POST: LibraryController/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, Models.DomainModels.Library library)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    _context.Entry(library).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
                    await _context.SaveChangesAsync();
                    return RedirectToAction(nameof(Index));
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError("", $"Error: {ex.Message}");
                    return View(library);
                    //throw;
                }
            }
            return View(library);
        }

        // GET: LibraryController/Delete/5
        public async Task<IActionResult> Delete(int id)
        {
            return View(await _context.Libraries.FindAsync(id));
        }

        // POST: LibraryController/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Delete(int id, Models.DomainModels.Library library)
        {
            try
            {
                //var result = await _context.Libraries.FindAsync(library.LibraryId);
                //if (result == null)
                //{
                //    return NotFound();
                //}
                //_context.Libraries.Remove(result);
                //await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            catch(Exception ex)
            {
                ModelState.AddModelError("", $"Error: {ex.Message}");
                return View();
            }
        }
    }
}
