﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Models.DomainModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.Controllers
{
    public class BorrowingController : Controller
    {
        private readonly LMSContext.Context.LMSDBContext _context;

        public BorrowingController(LMSContext.Context.LMSDBContext context)
        {
            _context = context;
        }
        // GET: BorrowingController
        public async Task<IActionResult> Index()
        {
            return View(await _context.Borrowings.Include(a=>a.Penalty).Include(b=>b.Book).Include(c=>c.Borrower).ToListAsync());
        }

        // GET: BorrowingController/Details/5
        public async Task<IActionResult> Details(int id)
        {
            return View(await _context.Borrowings.Where(a=>a.BorrowingId == id).Include(b=>b.Penalty).Include(c=>c.Book).Include(d=>d.Borrower).FirstOrDefaultAsync());
        }

        // GET: BorrowingController/Create
        public async Task<IActionResult> Create()
        {
            var book = await _context.Books.Where(a => a.IsBookAvailable == Models.DomainModels.BookAvailablity.Available).ToListAsync();
            book.Insert(0, new Book { BookAuthor = "abc", BookName = "Select A Book", BookId = 0 });
            var borrower = await _context.Borrowers.ToListAsync();
            borrower.Insert(0, new Borrower { BorrowerId = 0, BorrowerName = "Select A Borrower" });
            ViewBag.book = book;
            ViewBag.borrower = borrower;
            return View();
        }

        // POST: BorrowingController/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(Models.DomainModels.Borrowing vmborrowing)
        {
            if (vmborrowing.BookId == 0 || vmborrowing.BorrowerId == 0)
            {
                ModelState.AddModelError("", $"Error: You forget to select a Book or Borrower !!!");
                var book = await _context.Books.Where(a => a.IsBookAvailable == Models.DomainModels.BookAvailablity.Available).ToListAsync();
                book.Insert(0, new Book { BookAuthor = "abc", BookName = "Select A Book", BookId = 0 });
                var borrower = await _context.Borrowers.ToListAsync();
                borrower.Insert(0, new Borrower { BorrowerId = 0, BorrowerName = "Select A Borrower" });
                ViewBag.book = book;
                ViewBag.borrower = borrower;
                return View(vmborrowing);
            }
            if (vmborrowing.StartDate.IsStartingDayAWeekender())
            {
                ModelState.AddModelError("", $"Error: You cannot lend books on weekends (:");
                var book = await _context.Books.Where(a => a.IsBookAvailable == Models.DomainModels.BookAvailablity.Available).ToListAsync();
                book.Insert(0, new Book { BookAuthor = "abc", BookName = "Select A Book", BookId = 0 });
                var borrower = await _context.Borrowers.ToListAsync();
                borrower.Insert(0, new Borrower { BorrowerId = 0, BorrowerName = "Select A Borrower" });
                ViewBag.book = book;
                ViewBag.borrower = borrower;
                return View(vmborrowing);
            }
            if (ModelState.IsValid)
            {
                using (var transaction =await _context.Database.BeginTransactionAsync())
                {
                    try
                    {
                        var _borrowing = new Models.DomainModels.Borrowing();
                        _borrowing.BookId = vmborrowing.BookId;
                        _borrowing.BorrowerId = vmborrowing.BorrowerId;
                        _borrowing.StartDate = vmborrowing.StartDate;
                        _borrowing.EndDate = vmborrowing.StartDate.GetEndDateAfterSevenBusinessDays();

                        var _book = await _context.Books.FindAsync(vmborrowing.BookId);
                        _book.IsBookAvailable = BookAvailablity.Borrowed;
                        _context.Entry(_book).State = EntityState.Modified;

                        await _context.Borrowings.AddAsync(_borrowing);
                        await _context.SaveChangesAsync();
                        await transaction.CommitAsync();
                        return RedirectToAction(nameof(Index));
                    }
                    catch (Exception ex)
                    {
                        await transaction.RollbackAsync();
                        ModelState.AddModelError("", $"Error: {ex.Message}");
                        return View(vmborrowing);
                        //throw;
                    }
                }
            }
            return View(vmborrowing);
        }

        // GET: BorrowingController/Edit/5
        public async Task<IActionResult> Edit(int id)
        {
            return View(await _context.Borrowings.Where(a=>a.BorrowingId == id).Include(b=>b.Borrower).Include(c=>c.Book).FirstOrDefaultAsync());
        }

        // POST: BorrowingController/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, Models.DomainModels.Borrowing borrowing)
        {
            // the below will allow received date to be greater than both start and end date.
            // the bowworings can be penalized or not penalized.
            //if (borrowing.ReceivedDate < borrowing.StartDate)
            //{
            //    ModelState.AddModelError("", "Please Select Receiving Date later than start Date");
            //    borrowing = await _context.Borrowings.Where(a => a.BorrowingId == borrowing.BorrowingId).Include(b => b.Borrower).Include(c => c.Book).FirstOrDefaultAsync();
            //    return View(borrowing);
            //}

            // The below block is to ensure every borrowing is penalized as required in the task.
            // otherwise just comment out the below if block and uncomment the above if block
            if (borrowing.EndDate > borrowing.ReceivedDate)
            {
                ModelState.AddModelError("", "Please Select Receiving Date later than Ending Date");
                borrowing = await _context.Borrowings.Where(a => a.BorrowingId == borrowing.BorrowingId).Include(b => b.Borrower).Include(c => c.Book).FirstOrDefaultAsync();
                return View(borrowing);
            }
            if (borrowing.ReceivedDate == null)
            {
                ModelState.AddModelError("", "Please Select Receiving Date");
                borrowing = await _context.Borrowings.Where(a => a.BorrowingId == borrowing.BorrowingId).Include(b => b.Borrower).Include(c => c.Book).FirstOrDefaultAsync();
                return View(borrowing);
            }
            if (ModelState.IsValid)
            {
                using (var transaction = await _context.Database.BeginTransactionAsync())
                {
                    try
                    {
                        var _borrowing = await _context.Borrowings.FindAsync(borrowing.BorrowingId);
                        _borrowing.ReceivedDate = borrowing.ReceivedDate;

                        var _book = await _context.Books.FindAsync(borrowing.BookId);
                        _book.IsBookAvailable = BookAvailablity.Available;
                        _context.Entry(_book).State = EntityState.Modified;
                        if (borrowing.ReceivedDate > borrowing.EndDate)
                        {
                            var penalty = new Models.DomainModels.Penalty();
                            penalty.BorrowingId = borrowing.BorrowingId;
                            penalty.PenaltyDays = (borrowing.ReceivedDate - borrowing.EndDate).Value.Days;
                            penalty.PenaltyAmount = penalty.PenaltyDays * 100;
                            var result = await _context.Penalties.AddAsync(penalty);
                            _borrowing.Penalty = penalty;
                        }
                        await _context.SaveChangesAsync();
                        await transaction.CommitAsync();
                        return RedirectToAction(nameof(Index));
                    }
                    catch(Exception ex)
                    {
                        await transaction.RollbackAsync();
                        ModelState.AddModelError("", $"Error: {ex.Message}");
                        return View(borrowing);
                    }
                }
            }
            return View(borrowing);
        }

        // GET: BorrowingController/Delete/5
        public async Task<IActionResult> Delete(int id)
        {
            return View(await _context.Borrowings.FindAsync(id));
        }

        // POST: BorrowingController/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                //var result = await _context.Libraries.FindAsync(library.LibraryId);
                //if (result == null)
                //{
                //    return NotFound();
                //}
                //_context.Libraries.Remove(result);
                //await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", $"Error: {ex.Message}");
                return View();
            }
        }
    }
}
