﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.Controllers
{
    public class PenaltyController : Controller
    {
        private readonly LMSContext.Context.LMSDBContext _context;

        public PenaltyController(LMSContext.Context.LMSDBContext context)
        {
            _context = context;
        }
        // GET: PenaltyController
        public async Task<IActionResult> Index()
        {
            return View( await _context.Penalties.Include(a=>a.Borrowing).ThenInclude(b=>b.Borrower).ToListAsync());
        }

        // GET: PenaltyController/Details/5
        public async Task<IActionResult> Details(int id)
        {
            var result = await _context.Penalties.Where(a => a.PenaltyId == id).Include(b => b.Borrowing).ThenInclude(d => d.Book)
                .Include(c => c.Borrowing).ThenInclude(e => e.Borrower).FirstOrDefaultAsync();
            return View(result);
        }

        // GET: PenaltyController/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: PenaltyController/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: PenaltyController/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: PenaltyController/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: PenaltyController/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: PenaltyController/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
    }
}
