﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.Controllers
{
    public class BorrowerController : Controller
    {
        private readonly LMSContext.Context.LMSDBContext _context;

        public BorrowerController(LMSContext.Context.LMSDBContext context)
        {
            _context = context;
        }
        // GET: BorrowerController
        public async Task<IActionResult> Index()
        {
            return View(await _context.Borrowers.ToListAsync());
        }

        // GET: BorrowerController/Details/5
        public async Task<IActionResult> Details(int id)
        {
            return View(await _context.Borrowers.FindAsync(id));
        }

        // GET: BorrowerController/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: BorrowerController/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(Models.DomainModels.Borrower borrower)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    await _context.Borrowers.AddAsync(borrower);
                    await _context.SaveChangesAsync();
                    return RedirectToAction(nameof(Index));
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError("", $"Error: {ex.Message}");
                    return View(borrower);
                    //throw;
                }
            }
            return View(borrower);
        }

        // GET: BorrowerController/Edit/5
        public async Task<IActionResult> Edit(int id)
        {
            return View(await _context.Borrowers.FindAsync(id));
        }

        // POST: BorrowerController/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, Models.DomainModels.Borrower borrower)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    _context.Entry(borrower).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
                    await _context.SaveChangesAsync();
                    return RedirectToAction(nameof(Index));
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError("", $"Error: {ex.Message}");
                    return View(borrower);
                    //throw;
                }
            }
            return View(borrower);
        }

        // GET: BorrowerController/Delete/5
        public async Task<IActionResult> Delete(int id)
        {
            return View(await _context.Borrowers.FindAsync(id));
        }

        // POST: BorrowerController/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                //var result = await _context.Libraries.FindAsync(library.LibraryId);
                //if (result == null)
                //{
                //    return NotFound();
                //}
                //_context.Libraries.Remove(result);
                //await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", $"Error: {ex.Message}");
                return View();
            }
        }
    }
}
